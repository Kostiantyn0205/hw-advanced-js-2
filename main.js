/*При обробці введення користувача можуть виникати помилки через некоректні дані. За допомогою try...catch можна перехоплювати такі помилки та повідомляти користувача про проблеми.*/
/*Під час перевірки вхідних даних на відповідність певним умовам (наприклад, формат email, довжина пароля тощо) можна використовувати try...catch для перехоплення помилок при неправильних значеннях.*/
/*При використанні операцій, які можуть бути витратними по пам'яті (наприклад, великі масиви або цикли з великою кількістю ітерацій).*/
/*При парсингу даних, наприклад, аналіз JSON або інших форматів, можливі помилки через некоректну структуру даних. За допомогою try...catch можна обробляти винятки та запобігати збою програми.*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function isValidBook(book) {
    const requiredProperties = ["author", "name", "price"];
    return requiredProperties.filter(prop => !(prop in book));
}

function generateList() {
    const rootElement = document.getElementById("root");
    const ulElement = document.createElement("ul");

    books.forEach(book => {
        const liElement = document.createElement("li");
        try {
            const missingProps = isValidBook(book);
            if (missingProps.length > 0) {
                throw new Error(`Об'єкт ${JSON.stringify(book)} має неправильний формат. Відсутні властивості: ${missingProps.join(', ')}.`);
            }
            liElement.textContent = `Автор: ${book.author}, Назва: ${book.name}, Ціна: ${book.price}`;
            ulElement.appendChild(liElement);
        } catch (error) {
            console.error(error.message);
        }
    });
    rootElement.appendChild(ulElement);
}

generateList();